### Jambridge Linux installation

#### 1. Install portaudio 

Jambridge needs the portaudio library installed in your computer.
Use apt-get to install portaudio:

    sudo apt-get install portaudio19-dev

#### 2. Download Jambridge

Download the latest jambridge executable from this [link](https://bitbucket.org/claudio_ortega/jambridge/downloads),
and save it into a *new empty* directory:
            
    ~/jambridge            
    
#### 4. Launch Jambridge (for the first time)

Open a new terminal and type:

    cd ~/jambridge
    chmod +x ./jambridge*
    ./jambridge*
    
               
#### 5. Launch Jambridge (after the first time)
    
    cd ~/jambridge
    ./jambridge*


#### 6. Operation

Please refer to the instructions in [here](./jambridge-operation.md).



#### 7. Uninstall Jambridge

In case you need to uninstall the application, please open a terminal and type:
        
    rm -rf ~/jambridge


        