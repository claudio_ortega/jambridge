## Jambridge macOS installation

### Downloading Jambridge from the App Store

Open the `App Store` application, and click on `Discover` on the left.
Type in `Jambridge` in the `Search` box. After the download is finished
go to the `Applications` directory and click on `Jambridge`.
After Jambridge's window opens up, you may keep the `Jambridge` icon just showing up 
in the OS screen's dock, by right-clicking on it and selecting `Keep in Dock`.

![](./app_store_badge.jpg)

Follow up with the first time setup in [here](./jambridge-installation-macosx-setting-up.md).
