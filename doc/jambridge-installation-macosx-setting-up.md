## Jambridge macOS set up


### Set up permissions in latest macOS versions

Some of the latest macOS versions may need your permission before accessing resources.
These permission requests may happen at any time, but most likely when you try to connect for the first time. 
A window may pop up asking your permission for:

    - usage of the microphone, speakers, or any audio input/output in general
    - usage of the network


### Set up the screen notifications

Jambridge sends error notifications using an OS-wide screen notification mechanism.
It is important that you have it properly configured for Jambridge.
Please open `System Preferences/Notifications`, and select `Jambridge` from the list at the left.
Turn on `Allow notifications`, and select the `Alerts` option.


### Operation

Please refer to the instructions in [here](./jambridge-operation.md).


### Uninstall Jambridge

Using the `Finder application` drag and drop `Applications/Jambridge` into the `Trash`.


