### How to estimate the delay

The direct measurement of the delay requires special equipment.

We may still estimate based on the fact that the delay is the sum of:

    (a) The delay produced inside the computer in both sides, 
        which includes the delay in the audio drivers in the computer itself and 
        the delay in the external USB to audio interface equipment.
    (b) The delay in the network between participants.

The application-related portion `a` is only measurable in a lab setup.
Our measurements for different combinations of sampling frequency, buffer length, 
and USB audio interfaces, were around 15 ms.

The network portion `b` may be derived from the round trip delay estimation or measurement.
If the distance is less than 50 miles, then use 7mS-10mS as a good approximation for the round trip delay.
Otherwise, look for the closest cities from both sides in this place: `wondernetwork.com/pings`.
That site lists always round trip delays, so then use half of the value as we are attempting
to estimate delays in one direction.

Additionally and independently, Jambridge takes direct measurements of the round trip delay `b` 
in a continuous basis, and displays half of it under `Total delay (msec)`.
