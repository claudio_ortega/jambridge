### How to operate Jambridge

The application features a minimalistic design,
almost all the functionality is included in one simple to operate 
UI panel.

### Devices

Discover your audio devices at any time by clicking on the button `Discover`.
Jambridge will use all input channels on the selected audio device, and 
the first channel for the output direction. 


### Self test

Once you have your device selected, your next step is to test the correct operation of your audio devices.
Connect headphones into the output jack, select the input and output device, 
and press on the `Test` button under the `Audio Devices` section.
You should hear any signal in the input through the headphones by now.
If you need to adjust the volume then use the sound configuration panel in your 
MacOS `System preferences` panel, and/or the knobs in the USB audio adapter equipment.

The next step is to find for your setup the working combinations for 'Sampling frequency' (fs) and 'Buffer length' (bl).

The default values of fs=16000 and bl=128 will be probably adequate in most cases. 

Your safest sound production, but highest latency, will probably happen at fs=16000 and bl=512.
Your lowest latency, but least reliable, will occur with fs=44100 and bl=16.
Your best results will be somewhere in the middle, and you will have to experiment a little to find the sweet spot.


### Connecting with the studio

Once your device test is done, your next step is to connect with a studio.
Type in a nickname for you, and a make up a studio name. 
All people in the jam session should use the same studio name.
The studio name is a sort of secret passphrase for anyone invited to participate in the same jam session.

Then press the `Connect` button. and you should see all the other people in the studio.
Notice that the `Sampling frequency` and `Buffer length` are listed for anyone else already signed up.

Sampling frequency and buffer length need to be the same for all people in the studio.
All participants should share these values in order to select the lowest value for sampling frequency 
and the highest for buffer length.

For example, if musician A determined the best working combination for him was (44.1k, 128),
while musician B found it to be (32k,64), then both should move down
to the safest combination of both, that for the case would be (32k,128)

If you are a MacOS user, then follow the instructions on how to turn on notifications in the MacOS installation links
[installing from the AppStore](./jambridge-installation-MacOSx-using-app-store.md) or [downloading from outside the AppStore](./jambridge-installation-MacOSx-bypassing-app-store.md).
If these instructions are not followed, then your MacOS may block any Jambridge's error messages, 
causing confusion during operation.

You may notice that the internet latency changes over time,
and gives you different experiences across days or even during the same day. 


### Input gain sliders
You may control de mix over all incoming channels from the room except your own,
by changed the gain in the corresponding row in the studio table.


### VU-meters
You may monitor the energy level for each incoming channel and your own outgoing channel.



### Recording your sessions

You may record your sessions into files using a lossless sound format (aiff).
Jambridge creates those files inside the directory `Music` under your home directory,
and uses separate files for incoming and outgoing audio, so you may use them into a mix.
The audio will be captured in between `Start or resume recording` and `Close recording`. 
The `Pause recording` button will pause recording and the file's content will remain
intact until either you resume with `Start or resume recording` or you close the recording with 
`Close recording`. 
