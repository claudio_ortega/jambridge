
#### App Store delivery channel

You may download the latest version of Jambridge for free from the `Mac App Store`.

#### Release notes
        * 66c92f9        2025-02-01  (2.1.40) - Fix for Issue #24 - the vu-meters freeze - the app crashes
        * 5c2a4ae        2025-01-27  (2.1.39) - Fix for Issue #23 - Gain level shows wrong values after adding or removing entries in the room
        * 539cc67        2025-01-21  (2.1.38) - Fix for Issue #22 - Installer does not work
        * dc53e0f        2025-01-18  (2.1.37) - Fix for Issue #21 - Peer list does not clear after disconnect
        * 0756794        2025-01-14  (2.1.36) - Fix for Issue #18 - Closing the door in a laptop builds up delay.
                                              - Fix for Issue #15 - The app does not recover from switching in between wifi and no wifi.
        * d688091        2025-01-09  (2.1.35) - Fix for Issue #20 - the gain sliders do not remember the value after a disconnect/connect cycle.
                                              - Fix for Issue #19 - the vumeter indication should go to zero when there is no sound input.
                                              - Fix for Issue #16 - app stops receiving sound when laptop lid is closed for a while and then reopen.
        * 569c445        2025-01-05  (2.1.34) - Fix for Issue #17 - The latest versions do not work with older config files.
                                              - Fix for Issue #16 - The app stops receiving sound when laptop lid is closed and open.
        * e82a09e        2024-12-26  (2.1.33) - Separating UI tables for others and oneself.
        * e0bc749        2024-12-26  (2.1.32) - Make the mixing queue length configurable from the UI.
        * 8ea9236        2024-12-23  (2.1.31) - added vu-meters on network inputs and device output.      
                                              - changed the layout in the main pane.                  
                                              - rejects incoming data from unknown sources in the same room.           
                                              - added more logs in case of panics.                                     
                                              - the main window has a fixed size to prevent a bug in fyne UI library.
                                              - allowing only one registration per computer.
        * e2302fb        2024-12-07  (2.1.29) - added a new room map to registry to expedite queries.
                                              - added input level control into each incoming audio stream.
                                              - all available inputs are summed up on audio inpout devices.
                                              - fixed an issue on auto connect produced by the home router NAT.
        * df00a4f        2024-11-10  (2.1.27) - removing registration button, and some minor fixes.
        * 2457551        2023-07-15  (2.1.25) - changing the filter stop in the sink to 5KHz.
        * 0b62e56        2023-07-12  (2.1.24) - fixed invisible labels in dark mode.
        * 77558c6        2023-07-10  (2.1.23) - fixing connect buttons validation.
        * 7fa73b4        2023-06-03  (2.1.22) - enabled test tone while connected. 
                                              - added low pass filtering. 
                                              - removed connection signalling tone.
        * 8acc93e        2023-05-30  (2.1.21) - adding optional low pass filter, and fixed an issue introduced in 2.1.20
        * ded8b66        2023-05-29  (2.1.20) - preventing crashes by using the golang recover mechanism
        * 82beb0f        2023-05-18  (2.1.19) - querying the room every 30 seconds, enabling registration during a connection 
        * 40240b8        2023-05-05  (2.1.18) - persisting all UI controls across restart cycles
        * 216032c        2023-05-04  (2.1.17) - making sink loop go faster 
                                              - added sound signaling when connecting
        * 301baa9        2023-05-02  (2.1.16) - cleanup of all button listeners
                                              - improving the mixing algorithm in case of overrun
        * ca47d31        2023-05-01  (2.1.15) - improving the visibility for the mixer state in the UI
                                              - improving the mixing algorithm in case of only one source
        * ec0a7fc        2023-04-30  (2.1.14) - improving the mixing algorithm in case of overrun
                                              - the vessel mixer does not average vessels anymore, it sums
        * eecafb5        2023-04-30  (2.1.13) - added single buttons for start/stop, 
                                              - fixed auto registering to the studio, 
                                              - adding the mixer queue lengths in the stats
        * 2342e6a        2023-04-27  (2.1.12) - fixed unsizable window in M1
        * 0fd986f        2023-04-27  (2.1.11) - fixed vessels mixing algorithm and add re-requery of the studio on new sources
        * 96b3d9f        2023-04-22  (2.1.10) - minor cleanup
        * ecadf7f        2023-04-21  (2.1.9) - fixed stats
        * c314cf5        2023-04-21  (2.1.8) - fixed issue with more than two musicians
        * 10cc053        2023-04-18  (2.1.7) - some logs removed from critical timed loops
        * ef94fcf        2023-04-17  (2.1.6) - test tone goes additive, auto device discovery, fixed missing logs, minor UI improvements
        * 1abaafa        2023-04-03  (2.1.5) - UI - change Help/About tab label and content
        * d031e62        2023-03-31  (2.1.4) - UI general labels format cleanup
        * 390bec5        2023-03-30  - fix: prevent fyne library bug in case of M1 chip
        * 9a3b448        2023-03-30  - fix: self test start button remains disabled when it shouldn't
        * 3adf5a7        2023-03-30  - fix: the button 'find people in the room' should be always blue when enabled
        * 30db872        2023-03-29  - updated the notes after app store submission for review
        * 8aab580        2023-03-29  (2.1.3) - fix issue: discover devices button should be blue when there is nothing selected yet
        * 661913e        2023-03-29  - fix issue: resizing the main window crashes the app
        * 2ec3445        2023-03-27  (2.1.2) - fix for enable state of connect-to-room button is not working plus some minor adjustments
        * 6923443        2023-03-27  - fix for clicking the connect button while already connected makes the app unusable
        * 6d14f0e        2023-03-27  (2.1.1) - Removing license code and adding early log configuration.
        * 2c90305        2023-03-25  (2.1.0)  - Fixed silent crash in Ventura - using new certificates to sign the app in the store
        * 543b76b        2021-05-15  (2.0.0)  - support for more than 2 jamming sessions simultaneously
        * 63945d6        2021-04-21  (1.0.13) - small cleanup in build.sh
        * 8049527        2021-04-19  - adding a checkbox for starting the app in test mode
        * bd1a6d8        2021-04-19  - adding colored labels for send/receive status
        * fe274ab        2021-04-18  - fixed rediscovering of audio devices on the fly
        * bb360ac        2021-04-18  - fixed issue of lost focus when coming back from the open recording button
        * 0632458        2021-04-16  (1.0.12) - adding a checkbox to indicate using lan addresses only and adding better audio state displays
        * 51940b9        2021-04-15  (1.0.11) - dummy commit needed to move to 1.0.11
        * 2b21ec7        2021-04-15  (1.0.10) - adding more clarity to the labels and buttons
        * daf9615        2021-04-12           - using the register button to query the room as well
        * 8c23c08        2021-03-22  (1.0.9)  - allowing uppercase anywhere in some of the UI entries
        * 99cfe25        2021-03-22           - reveal the recording directory via Finder, and not via clipboard usage
        * e1c7200        2021-03-21           - the number of files reported in the music recording directory is off
        * 04f95d9        2021-03-19  (1.0.8)  - getting the app store version directly from git
        * 90d7c66        2021-03-19           - disconnection ends the recording session
        * dbb878f        2021-03-18  (1.0.7)  - changing the earliest OS version supported to High Sierra (10.13) and bumping up the App Store version to 1.0.7
        * 90a7358        2021-03-18           - making the build date in the .pkg file be the commit date




