### Credits

To friends and family in the list below, in alphabetical order,
who helped Jambridge to exist through their inspiration, encouragement,
excellent ideas, and the best quality assurance.
Thanks to you all !! (in alphabetical order):

    Charlie Davis    
    Daniel Chapiro (special mention)
    Esteban Castro
    Facundo Ortega
    Horacio Franco
    Leah Rogers
    Lien Davis
    Santiago Ortega
    Vicki Benavente


Thanks to the people in all of these open source projects, 
Jambridge wouldn't exist without their efforts.

    Portaudio C language audio library - Copyright (c) 1999-2006 Ross Bencina and Phil Burk
    Portaudio Go language audio library - Copyright (c) 2017 Gordon Klaus
    Fyne Go language GUI library - Copyright (c) 2018 Fyne.io developers
    The Go language contributors - https://golang.org/CONTRIBUTORS



### Dedications

We dedicate Jambridge to all the musicians of the world.
