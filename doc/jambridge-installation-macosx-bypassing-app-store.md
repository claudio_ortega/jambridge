## Jambridge macOS installation

### Downloading Jambridge outside Apple's App Store

You may also download the latest jambridge executable from this [link](https://bitbucket.org/claudio_ortega/jambridge/downloads),
and save it into your desktop.
Then right-click on the icon and select `Open`, and follow the prompts to give your permission to install.

Follow up with the first time setup in [here](./jambridge-installation-macosx-setting-up.md).
