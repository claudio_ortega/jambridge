## Privacy policy notice

Jambridge registers your public IP address and the nickname you choose into 
an ephemeral session registration server.
Only this server and your partner's Jambridge client app will ever know your IP address.
The registration happens at the moment you sign up into the room.
It is not stored on the Jambridge's server hard disk or in your partners' hard disk,
and it gets deleted for good from memory in the server and in your partner's laptop
when the session ends.

In addition, this information is not shared with any third party, neither used in any other way nor purpose.

