## Jambridge (R) - The low latency, CD-quality, distributed music creation tool

### What is Jambridge 

Jambridge creates an audio studio that eliminates the very excessive time delay or latency, 
present in video conferencing applications.

Imagine creating an exclusive studio where musicians at separate locations may jam, play, 
or teach music over the Internet with very low latency and CD-quality sound.

Jambridge is the easiest tool of its kind to set up and operate.

Jambridge eliminates some non-essential operational complications and performance degradation
factors present in other tools, such as Jamulus or Jack. 

With Jambridge:

- There is no need to touch your home wi-fi router, as Jambridge does not need you to open internal
  ports via setup, or do any other special tweaking in your router configuration.
 
- There is no need for any accounts in Jambridge. No signing up, no user, no password.

- Jambridge communicates partners directly without any intermediate servers. Latency is reduced,
  playing feels natural, and there is no need for participants to listen to a delayed feed of their own 
  sound sent back to them, as it is the case with some alternative apps.

Jambridge conveys pure and uncompressed CD-quality 44.1kHz/16 bits digitized sound over 
regular broadband connections with very low latency. When sites are located in the same metro area and 
are meeting the recommended setup, then the latency could be as low as 25-30 ms.

You may learn more about Jambridge very low latency in [here](./jambridge-latency.md), 
and in [here](https://bit.ly/3XS5iR4) for an independent latency comparison in between Jambridge and Jacktrip. 

You may use Jambridge without special hardware on a laptop or a desktop under either macOS or Linux.
You may also enjoy its low latency audio in conjunction with a video app like Zoom;
muting the sound in the video app, so your audio flows only through Jambridge.


### Recommended operating conditions

Jambridge offers a latency in between 25 and 30 ms between any pair of musicians in the studio, 
provided they meet the following operating conditions:
Required:
- All participants are running macOS, any version from earlier High Sierra up to the latest version,
  or any Linux distribution.
- All participants are located in the same geographical metro area.
- All participants use headphones.
  
Highly recommended:
- All participants are using a USB audio interface.
- All participants are using a wired connection with the home router.

There is a huge performance gain if you use both a USB audio interface and a wired connection to your router.
The difference in the delay in between operating on recommended conditions and operating on non-recommended conditions
could be in the order of a hundred milliseconds.

Most new desktops/laptops are adequate, and it is acceptable to try Jambridge for the first time using as audio input
the laptop's built-in microphone; and for the audio output a pair of plugged headphones.

However, and this is important, in order to achieve the minimum delay and best sound quality,
you should use an external audio interface connected with your laptop via USB,
and have your laptop connected to the internet via a wired connection.

There are several alternatives for USB audio interfaces in the 50-150$ price range. 
We have tested Jambridge with all the devices listed in the reference section,
and there are many other options beyond that list.

We recommend you use flat ethernet cat5 wires for the wired connection in between your laptop and your wifi router. 
Those cables are both reliable and much easier to handle than the non-flat alternative. 
See the options listed in the reference section at the end.


### Trying out under non-recommended conditions

You can try Jambridge under the non-recommended conditions, especially when you want to get a feeling of its low latency.
However, if this does not bring the expected results, then fall back into the recommended setup.



### How to download and install 

Please follow the installation steps for either
[macOS using the AppStore](./jambridge-installation-macosx-using-app-store.md) or
[macOS bypassing the AppStore](./jambridge-installation-macosx-bypassing-app-store.md) or
[Linux](./jambridge-installation-linux.md).


### How to operate 

Jambridge is very easy to set up and operate.
Please read the instructions [here](./jambridge-operation.md).

### Customer privacy policy 

Jambridge take your privacy very seriously. Please read our policy [here](./jambridge-privacy-notice.md).


### Customer support

We love for you to have a seamless experience. 
Please read on our customer support policy [here](./jambridge-support.md).


### Release notes

The release notes are published in [here](./release-notes.md).


### Known issues

Known issues are recorded in Jira under this link [here](https://bitbucket.org/claudio_ortega/jambridge/issues).
We make our best effort to fix all serious issues by the next fixing release.


### Trademark 

Jambridge(R) is a registered trademark of Claudio Ortega.


### Credits

Please find the credits [here](./jambridge-credits.md).


### References

[1] [PreSonus AudioBox USB96](https://smile.amazon.com/PreSonus-AudioBox-USB-Recording-System/dp/B071W6YVDR)

[2] [Focusrite Scarlett 2i2](https://smile.amazon.com/Focusrite-Scarlett-Audio-Interface-Tools/dp/B01E6T56EA)

[3] [Behringer U-phoria UM2](https://www.behringer.com/product.html?modelCode=P0AVV)

[4] [Flat ethernet cable](https://smile.amazon.com/Cat-Ethernet-Cable-White-Connectors/dp/B00WD017GQ)





