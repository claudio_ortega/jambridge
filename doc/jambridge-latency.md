### The factors impacting the latency ###

Unlike other internet communication tools, and solely because of its very low latency,
Jambridge is designed specifically for making music over the Internet.

Imagine that you are in a jamming session in normal conditions
with a separation of about 20 to 30 feet in between your partner and yourself.
The latency you would perceive from the sound coming from your partner will be approximately
20 to 30 ms (as the sound travels close to 1ms per foot).

However, if you were at different locations using a conference/telephony application over the open Internet,
the latency in between each other might most likely be 250 ms or more.
Depending on the person and the music material, it is apparent the maximum tolerable latency for making
music simultaneously is around 30-50 ms.
That is why tools like Zoom, Skype, or WhatsApp are even a plain phone line are all
unsuitable for making music simultaneously over the Internet.

It is not possible to guarantee for the latency to remain under a value 
suitable for simultaneous music production. This is because of two reasons:
when the distance in between parties is long, and/or because of abnormal conditions of the network in between,
even in the case of short distances.

Additionally, the Internet routers produce jitter: a short-term variation in the latency around a long-term baseline.

For example, the latency in-between San Francisco and Redwood City could be around 5 ms long-term,
but it is possible that over some window of a few seconds such latency could become 50 ms. Such a variation poses an extra challenge.

The latency perceived in between two musicians in any particular scenario will depend on many factors, 
including but not limited to the following: 

    1 - the computing power in the laptop/desktop on both sides
    2 - how much are the other processes consuming computing resources 
    3 - how are both computers connected to their home internet router (ie: wired vs. wireless) 
    4 - how is the internet topology in between both sites, (i.e.: the number of route hops and service provider's boundaries in between) 
    5 - any extra delay inside the audio equipment, specially if not using the recommended USB audio interfaces.

You may learn more on how to estimate the latency in [here](./jambridge-latency-estimation.md).


### How does Jambridge achieve low latency

Jambridge achieves its low latency by connecting all sides directly,
with no extra servers in between, and without returning any of sound back to the sender.

Because of this approach, Jambridge may offer as low as 25 ms latency with excellent repeatability and accuracy.


### How do other applications achieve low latency

Other applications use the strategy of mixing the sound from all participants in an intermediate
server and returning the mix back to everyone.

This approach produces two undesirable effects:

(1) An extra increase in the latency: the information needs to travel from one side A to some intermediate server,
and then back to the other side B.

(2) Musicians perceive an annoying latency in between their own vision/propioception and their audio perceptions,
as their own sound is first forwarded from, say, A to the intermediate server, and from the intermediate server 
sent back to A.

Jambridge does not do any of this. The digitized sound goes directly in between A and B, 
without the use of any intermediary.